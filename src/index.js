//Data Calculator
var startDate = "";
var endDate = "";

//convert input data string from DD/MM/YYYY to date object
function parseDate(str) {
  var dmy = str.split("/");
  var date = new Date(dmy[2], dmy[1], dmy[0]);
  console.log(date);
  return date;
}
//calculate the difference btw input dates
function datediff(start, end) {

  return Math.abs(Math.round((end - start) / (1000 * 60 * 60 * 24))) - 1;
}
//read input from command line
const readline = require("readline");
const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

r1.question(
  "Please input start Date,end Date eg.'DD/MM/YYYY,DD/MM/YYYY'",
  answer => {
    startDate = parseDate(answer.split(",")[0]);
    endDate = parseDate(answer.split(",")[1]);
    console.log(
      "Start Date: " +
        answer.split(",")[0] +
        " End Date: " +
        answer.split(",")[1]
    );
    console.log("result: " + datediff(startDate, endDate) + " day(s)");
    r1.close();
  }
);
